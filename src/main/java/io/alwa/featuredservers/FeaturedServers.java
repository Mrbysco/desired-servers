
package io.alwa.featuredservers;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ServerData;
import net.minecraft.client.multiplayer.ServerList;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.lifecycle.InterModEnqueueEvent;
import net.minecraftforge.fml.event.lifecycle.InterModProcessEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.lang.reflect.Type;
@Mod("featuredservers")
public class FeaturedServers {
    private static final Logger LOGGER = LogManager.getLogger();
    private static String FMLConfigFolder;
    private FileReader serversFile;

    public FeaturedServers() {
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::onConfigLoad);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::doClientStuff);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::enqueueIMC);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::processIMC);
    }

    private void setup(final FMLCommonSetupEvent event) {
        LOGGER.debug("Forge wanted this setup event to exist");
    }

    private void enqueueIMC(final InterModEnqueueEvent event) {
        LOGGER.debug("Forge wanted this enqueue event to exist");
    }

    private void processIMC(final InterModProcessEvent event) {
        LOGGER.debug("Forge wanted this Process event to exist");
    }

    private void doClientStuff(FMLClientSetupEvent event) {
        File featuredServerList;
        File configFolder = new File(FMLConfigFolder, "config/FeaturedServers");
        if (!configFolder.exists()) {
            configFolder.mkdirs();
        }
        if (!(featuredServerList = new File(configFolder, "featuredservers.json")).exists()) {
            try {
                featuredServerList.createNewFile();
                FileWriter writer = new FileWriter(featuredServerList);
                writer.write("[\n  {\n    \"serverName\": \"Featured Server\",\n    \"serverIP\": \"127.0.0.1\",\n    \"forceResourcePack\": \"true\"\n  },\n  {\n    \"serverName\": \"Another Server!\",\n    \"serverIP\": \"192.168.1.1\",\n    \"forceResourcePack\": \"false\"\n  }\n]");
                writer.close();
            } catch (Throwable t) {
                LOGGER.log(Level.INFO,"Could create the default config file for Featured Servers Deus Ex\nException was: "+t.getMessage());
                return;
            }
        }

        try {
            this.serversFile = new FileReader(featuredServerList.getPath());
        }
        catch (Throwable t)  {
            LOGGER.log(Level.INFO,"Could create the default config file for Featured Servers Deus Ex\nException was: "+t.getMessage());
            return;
        }
        Gson gson = new Gson();
        JsonReader reader = new JsonReader(this.serversFile);
        ServerDataHelper[] featuredList = (ServerDataHelper[])gson.fromJson(reader, (Type)((Object)ServerDataHelper[].class));
        if (featuredList == null) return;
        ServerList serverList = new ServerList(Minecraft.getInstance());
        int n = featuredList.length;
        int n2 = 0;
        while (n2 < n) {
            ServerDataHelper serverhelp = featuredList[n2];
            ServerData server = new ServerData(serverhelp.serverName, serverhelp.serverIP, false);
            if (serverhelp.forceResourcePack != null && serverhelp.forceResourcePack) {
                server.setResourceMode(ServerData.ServerResourceMode.ENABLED);
            }
            if (FeaturedServers.inList(server, serverList)) {
                LOGGER.log(Level.INFO, "Featured server already in server list");
            } else {
                LOGGER.log(Level.INFO, "Adding featured server");
                serverList.addServerData(server);
                serverList.saveServerList();
            }
            ++n2;
        }
    }

    void onConfigLoad(ModConfig.Loading event) {
        FMLConfigFolder = event.getConfig().getFullPath().getParent().toString();
    }

    public static Boolean inList(ServerData server, ServerList list) {
        if (list == null) {
            return false;
        }
        int i = 0;
        while (i < list.countServers()) {
            ServerData data = list.getServerData(i);
            if (data.serverIP != null &&
                    data.serverIP.equalsIgnoreCase(server.serverIP) &&
                    data.serverName != null &&
                    data.serverName.equalsIgnoreCase(server.serverName)
             ) {
                return true;
            }
            ++i;
        }
        return false;
    }

    public static class ServerDataHelper {
        public String serverName;
        public String serverIP;
        public Boolean forceResourcePack;
    }

}

