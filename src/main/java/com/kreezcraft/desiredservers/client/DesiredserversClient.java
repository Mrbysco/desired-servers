package com.kreezcraft.desiredservers.client;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ServerInfo;
import net.minecraft.client.option.ServerList;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.lang.reflect.Type;

@Environment(EnvType.CLIENT)
public class DesiredserversClient implements ClientModInitializer {
    private FileReader serversFile;
    @Override
    public void onInitializeClient() {
        File desiredServerList;
        File configFolder = new File(FabricLoader.getInstance().getConfigDirectory(), "desiredServers");
        if (!configFolder.exists()) {
            configFolder.mkdirs();
        }
        if (!(desiredServerList = new File(configFolder, "desiredservers.json")).exists()) {
            try {
                desiredServerList.createNewFile();
                FileWriter writer = new FileWriter(desiredServerList);
                writer.write("[\n  {\n    \"serverName\": \"Desired Server\",\n    \"serverIP\": \"127.0.0.1\",\n    \"forceResourcePack\": \"true\"\n  },\n  {\n    \"serverName\": \"Another Server!\",\n    \"serverIP\": \"192.168.1.1\",\n    \"forceResourcePack\": \"false\"\n  }\n]");
                writer.close();
            } catch (Throwable t) {
                System.err.println("Could create the default config file for Desired Servers Deus Ex\nException was: "+t.getMessage());
                return;
            }
        }

        try {
            this.serversFile = new FileReader(desiredServerList.getPath());
        }
        catch (Throwable t)  {
            System.err.println("Could create the default config file for Desired Servers Deus Ex\nException was: "+t.getMessage());
            return;
        }
        Gson gson = new Gson();
        JsonReader reader = new JsonReader(this.serversFile);
        ServerDataHelper[] desiredList = gson.fromJson(reader, ServerDataHelper[].class);
        if (desiredList == null) return;
        ServerList serverList = new ServerList(MinecraftClient.getInstance());
        int n = desiredList.length;
        int n2 = 0;
        while (n2 < n) {
            ServerDataHelper serverhelp = desiredList[n2];
            ServerInfo server = new ServerInfo(serverhelp.serverName, serverhelp.serverIP, false);
            if (serverhelp.forceResourcePack != null && serverhelp.forceResourcePack) {
                server.setResourcePackPolicy(ServerInfo.ResourcePackState.ENABLED);
            }
            if (inList(server, serverList)) {
                System.out.println("Desired server already in server list");
            } else {
                System.out.println("Adding desired server");
                serverList.add(server);
                serverList.saveFile();
            }
            ++n2;
        }
    }

    public static Boolean inList(ServerInfo server, ServerList list) {
        if (list == null) {
            return false;
        }
        int i = 0;
        while (i < list.size()) {
            ServerInfo data = list.get(i);
            if (data.address != null &&
                    data.address.equalsIgnoreCase(server.address) &&
                    data.name != null &&
                    data.name.equalsIgnoreCase(server.name)
            ) {
                return true;
            }
            ++i;
        }
        return false;
    }


    public static class ServerDataHelper {
        public String serverName;
        public String serverIP;
        public Boolean forceResourcePack;
    }

}
