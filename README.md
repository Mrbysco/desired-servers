[![](https://discordapp.com/api/guilds/96753964485181440/widget.png?style=banner2)](https://discord.gg/ngNQjT5) [![](https://s12.directupload.net/images/200916/joj33k55.png)](https://twitter.com/kreezxil) [![](https://s12.directupload.net/images/200916/efhmdjhg.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fwww.reddit.com%252fr%252fMinecraftModdedForge)

[![Bisect Hosting](https://www.bisecthosting.com/images/logos/dark_text@1538x500.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fkreezxil)

If you want a server setup for nearly any mod pack with zero effort, get a [server with BisectHosting](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fkreezxil) and receive 25% off your first month as a new customer using the code kreezxil

Join atleast [![](https://img.shields.io/discord/96753964485181440.svg?colorB=7289DA)](https://discord.gg/mYCWTZD) of us on Discord right now!

From a page that is GPLv3 licensed that sounds very familiar to this.

---

### Desired Servers

This is a mod that removes the need to ship a servers.dat file that overwrites player-added servers.

Stop shipping that file and add **Desired Servers**! You can add multiple servers with a simple json format.

On first run, the mod will generate a desiredservers.json with the following content if one isn't present:

\[
  {
    "serverName": "Desired Server",
    "serverIP": "127.0.0.1"
  },
  {
    "serverName": "Another Desired Server!",
    "serverIP": "192.168.1.1"
  }
\]

 Adding a server is as simple as adding another json block with the details.

The server is added to the server list on load, if it is already there it will be ignored.

![](https://i.imgur.com/1eqKb45.png)

Server list JSON

![](https://i.imgur.com/Jqn7Xco.png)

Servers in the multiplayer screen

### The TidBit

The Featured Servers/ folder now lives in the config folder.

### The What and How

Forge didn't like how the mod was laid out and wanted it to be more like the default example. So I added in some stuff for that. Also the mod was catching way too much stuff with reporting, so I unwrapped that added try/catch clauses where it was absolutely necessary.

### Other Info

\* Same license as the original GPLv3 at [https://www.curseforge.com/minecraft/mc-mods/featured-servers](https://www.curseforge.com/minecraft/mc-mods/featured-servers)

\* ported to 1.16.1

\* source now available

\* client only
